const moment = require('moment')
const {last} = require('lodash')

/*
calculateTreatmentEnd
- lastAlignerChangeDate (date): When the last aligner change was done
- treatmentStarted (boolean): If the treatment has been started
- treatmentDuration (int): Duration in days of the planned treatment
- alignerChanges (array): List of future planned aligner changes
*/

module.exports = function calculateTreatmentEnd(
  {lastAlignerChangeDate, treatmentStarted, treatmentDuration},
  alignerChanges
) {
  if (alignerChanges && alignerChanges.length > 0) {
    const today = moment()
    const lastAlignerChange = last(alignerChanges)
    const firstAlignerChange = alignerChanges[0]
    const plannedEnd = lastAlignerChange.date.clone().add(2, 'weeks')
    if (firstAlignerChange?.date?.isBefore(today)) {
      const expectedTreatmentLength = plannedEnd.diff(
        firstAlignerChange.date,
        'days'
      )
      return moment().add(expectedTreatmentLength, 'days')
    }
    return plannedEnd
  }

  if (!lastAlignerChangeDate) return
  return lastAlignerChangeDate
    .clone()
    .startOf('day')
    .add(2, 'weeks')
}
