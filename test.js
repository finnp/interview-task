const tk = require('timekeeper')
const moment = require('moment')
const calculateTreatmentEnd = require('./calculateTreatmentEnd')

describe('calculateTreatmentEnd end', () => {
  test('aligner changes still needed [1 1 5 5]', () => {
    expect(
      calculateTreatmentEnd(
        {
          lastAlignerChangeDate: moment('2119-01-01T10:00:00Z').utcOffset(0),
          treatmentStarted: true,
        },
        [
          {
            date: moment('2119-01-15T00:00:00.000Z').utcOffset(0),
            nextUpper: 2,
            nextLower: 2,
            photoCheckin: false,
          },
          {
            date: moment('2119-01-29T00:00:00.000Z').utcOffset(0),
            nextUpper: 3,
            nextLower: 3,
            photoCheckin: false,
          },
          {
            date: moment('2119-02-12T00:00:00.000Z').utcOffset(0),
            nextUpper: 4,
            nextLower: 4,
            photoCheckin: false,
          },
          {
            date: moment('2119-02-26T00:00:00.000Z').utcOffset(0),
            nextUpper: 5,
            nextLower: 5,
            photoCheckin: true,
          },
        ]
      )
    ).toMatchSnapshot()
  })

  test('no aligner changes needed [5 5 5 5]', () => {
    expect(
      calculateTreatmentEnd(
        {
          lastAlignerChangeDate: moment('2119-01-01T10:00:00Z').utcOffset(0),
          treatmentStarted: true,
        },
        []
      )
    ).toMatchSnapshot()
  })

  test('aligner changes way overdue/in the past [1 1 5 5]', () => {
    tk.freeze(new Date('2019-11-21'))

    expect(
      calculateTreatmentEnd(
        {
          lastAlignerChangeDate: moment('2019-01-01T10:00:00Z').utcOffset(0),
          treatmentStarted: true,
        },
        [
          {
            date: moment('2019-01-15T00:00:00.000Z').utcOffset(0),
            nextUpper: 2,
            nextLower: 2,
            photoCheckin: false,
          },
          {
            date: moment('2019-01-29T00:00:00.000Z').utcOffset(0),
            nextUpper: 3,
            nextLower: 3,
            photoCheckin: false,
          },
          {
            date: moment('2019-02-12T00:00:00.000Z').utcOffset(0),
            nextUpper: 4,
            nextLower: 4,
            photoCheckin: false,
          },
          {
            date: moment('2019-02-26T00:00:00.000Z').utcOffset(0),
            nextUpper: 5,
            nextLower: 5,
            photoCheckin: true,
          },
        ]
      )
    ).toMatchSnapshot()
    tk.reset()
  })

  test('next aligner change in the past, 4 weeks remaining [1 1 3 3]', () => {
    tk.freeze(new Date('2019-01-21'))

    expect(
      calculateTreatmentEnd(
        {
          lastAlignerChangeDate: moment('2019-01-01T10:00:00Z').utcOffset(0),
          treatmentStarted: true,
        },
        [
          {
            date: moment('2019-01-15T00:00:00.000Z').utcOffset(0),
            nextUpper: 2,
            nextLower: 2,
            photoCheckin: false,
          },
          {
            date: moment('2019-01-29T00:00:00.000Z').utcOffset(0),
            nextUpper: 3,
            nextLower: 3,
            photoCheckin: false,
          },
        ]
      )
    ).toMatchSnapshot()
    tk.reset()
  })

  test('one aligner step missing that is in the past [5 5 6 6]', () => {
    tk.freeze(new Date('2019-01-21'))

    expect(
      calculateTreatmentEnd(
        {
          lastAlignerChangeDate: moment('2019-01-01T10:00:00Z').utcOffset(0),
          treatmentStarted: true,
        },
        [
          {
            date: moment('2019-01-15T00:00:00.000Z').utcOffset(0),
            nextUpper: 6,
            nextLower: 6,
            photoCheckin: false,
          },
        ]
      )
    ).toMatchSnapshot()
    tk.reset()
  })

  test('no aligner changes left', () => {
    expect(
      calculateTreatmentEnd(
        {
          lastAlignerChangeDate: moment('2019-01-01T10:00:00Z').utcOffset(0),
          treatmentStarted: true,
        },
        []
      )
    ).toMatchSnapshot()
  })

  test('treatment has not started yet', () => {
    tk.freeze(new Date('2020-01-01'))

    expect(
      calculateTreatmentEnd(
        {
          treatmentStarted: false,
        },
        [
          {
            date: moment('2020-01-15T00:00:00.000Z').utcOffset(0),
            nextUpper: 2,
            nextLower: 2,
            photoCheckin: false,
          },
          {
            date: moment('2020-01-29T00:00:00.000Z').utcOffset(0),
            nextUpper: 3,
            nextLower: 3,
            photoCheckin: false,
          },
        ]
      )
    ).toMatchSnapshot()
    tk.reset()
  })
})
