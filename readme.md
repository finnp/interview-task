# Calculate Treatment End

At PlusDental patients receive a set of aligners that they need to change every two weeks. Depending on the
number of aligners we can estimate how long a treatment ideally lasts. For example a patient with 12 aligners
will take 24 weeks until the end of treatment. If a patient switches to the 7th aligner they will have
12 weeks left in the treatment.

The function in this repository takes as input data about the treatment and will calculate the expected treatment
end so we can display it to the patient in the app.

## Install

```js
npm install
```

## Tests

```js
npm test
```

## Lint

```
npm run lint
```

This will automatically format your code.
